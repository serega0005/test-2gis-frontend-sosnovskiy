'use strict';
$(document).ready(function () {
	var container = $('.globalWrapper');
	var source   = $('#entry-template').html();
	var template = Handlebars.compile(source);
	var n = 1;


	// Подставляем данные из файла и выводим html код на страницу 
	for (var one in cards) {
		let html    = template(cards[one]) ;
		container.append(html);
		container.find('.block__number').last().html(++one);
	}

	// Изменение фона при наведении на незанятую область
	$('body').on('mouseover', function (e) {
		if (!e.target.classList.contains('block__content')) {
		  $(this).css('background-color', '#e9e6d3');
		}
	});

	$('body').on('mouseout', function (e) {
		if (!e.target.classList.contains('block__content')) {
		  $(this).css('background-color', '#f6f2de');
		}
	});

	// Событие на клик и сочетание клавиш(удаление, добавление карточек)
	$('body').on('keydown click', function(e){
		if (e.shiftKey && e.altKey && e.type == 'click') { 
			addCard('narrow');
	  } else if (e.shiftKey && e.type == 'click') {
			addCard('wide');
		} else if (e.target.classList.contains('block__content') && e.type == 'click') {
			deleteCard();
		}
		return false;	
	});


	var addCard = function(e) {     			// функция добавления карточки	
		for (var one in cards) {
			if (cards[one]['type'] = e) {
				let html    = template(cards[one]) ;
				var ordNumber = $('.block').last().find('.block__number').html();
				container.append(html);

				if (ordNumber > 0) {   					// проверка на наличие карточек на старнице
					++ordNumber;
				} else {
					ordNumber = 1;
				}

				$('.block').last().find('.block__number').html(ordNumber);
				browserState();
				return false;	
			} 
		}
	};

	var deleteCard = function(e) {     		// функция удаления карточки
		$('body').find('.block').first().remove();
		browserState();
	};

	var browserState = function(e) {     // запись состояния страницы
		++n;
		var stateObj = { state: container.html() };
		history.pushState(stateObj, "page" + n, "state-" + n);
	};

	var updateState = function(e) {      // обновление контента на странице
		if (!history) return;
		container.html(history['state']['state']);
	};

	$(window).bind('popstate', function (e) {
		updateState();
	});

	var stateObj = { state: container.html() };  // добавление состояния при первой загрузке страницы
	history.pushState(stateObj, "page", "");
	updateState();
});




